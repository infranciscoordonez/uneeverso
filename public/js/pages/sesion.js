var sesion = function () {
	
	var iniciar = function() {
			$('#btn-acceder').attr('disabled', true);
			var usuario_steem_ = $('#usuario_steem').val();
			var contrasena_uneeverso_ = $('#contrasena_uneeverso').val();
			if (usuario_steem_=="" || contrasena_uneeverso_=="") {
				$('#btn-acceder').attr('disabled', false);
				alerta.error("Quedan campos vacios");
				return false;
			}
			isUserSteem(usuario_steem_, function (validacion_u_steem) {
				if (validacion_u_steem==true) {
					if (isValidaContrasena(contrasena_uneeverso_)) {
					    $.ajax({
							url: window.location.href+'/iniciar_sesion',
							type:'POST',	
							data:{	
									nombre_steem:usuario_steem_,
									contrasena_uneeverso:contrasena_uneeverso_,
								},
					        beforeSend: function () {

					        },
					        success:  function (resultado) {
				        	  	//console.log(JSON.stringify(resultado));
				        	  	if (resultado.error==false) {
				        	  		//alerta.exito(resultado.mensaje);
									localStorage.setItem("token", resultado.token);
									window.location.href='/@'+resultado.datos.nombre_steem+'/automatizacion';
				        	  	} else{
				        	  		alerta.error(resultado.mensaje);
				        	  	};
								$('#btn-acceder').attr('disabled', false);
					        },
					    	error:  function(error) {
								console.log(JSON.stringify(error));
						    }	 
						});
					} else{
						$('#btn-acceder').attr('disabled', false);
						alerta.error("Contraseña uneeverso tiene espacios");
					};
				}else{
					$('#btn-acceder').attr('disabled', false);
					alerta.error("EL usuario no existe en steem");
				};
			});	
	}
	var registrar_usuario = function () {
			var usuario_steem_ = $('#usuario_steem').val();
			var correo_electronico_ = $('#correo_electronico').val();
			var contrasena_uneeverso_ = $('#contrasena_uneeverso').val();			
			var priwif_steem_ = $('#priwif_steem').val();
			if (usuario_steem_=="" || contrasena_uneeverso_=="" ||
				 priwif_steem_=="" || correo_electronico_=="") {
				alerta.error("Quedan campos vacios");
				return false;
			}
			$('#btn-registro').attr('disabled', true);
			if (validar_correo(contrasena_uneeverso_)) {
				isUserSteem(usuario_steem_, function (validacion_u_steem) {
					if (validacion_u_steem!=false) {
						if (isValidaContrasena(contrasena_uneeverso_)) {
								if (iswif(priwif_steem_)!=false) {
									//Enviar datos para verificar y guardarlos
								    $.ajax({
										url: window.location.href+'/registrar_usuario',
										type:'POST',	
										data:{	
												nombre_steem:usuario_steem_,
												correo_electronico:correo_electronico_,
												contrasena_uneeverso:contrasena_uneeverso_,
												wif_post_priv_steem:priwif_steem_,
											},
								        beforeSend: function () {

								        },
								        success:  function (resultado) {
							        	  	//console.log(JSON.stringify(resultado));
							        	  	if (resultado.error==false) {
							        	  		//alerta.exito(resultado.mensaje);
							        	  		alertify.alert(
													"Cambio exitoso:",
													resultado.mensaje,
													function(){
												    	localStorage.clear();
														sessionStorage.clear();
														window.location.href="/login";
												});						        	  		
							        	  	} else{
							        	  		alerta.error(resultado.mensaje);
							        	  	};
											$('#btn-registro').attr('disabled', false);
								        },
								    	error:  function(error) {
											console.log(JSON.stringify(error));
									    }	 
									});
								}else{
									$('#btn-registro').attr('disabled', false);
									alerta.error("Wif priv the post steem errado");
								};
						} else{
							$('#btn-registro').attr('disabled', false);
							alerta.error("Contraseña uneeverso tiene espacios");
						};
					}else{
						$('#btn-registro').attr('disabled', false);
						alerta.error("Usuario no existe en steem");
					};
				});
			}else{
				$('#btn-registro').attr('disabled', false);
				alerta.error("Correo electrónico con formato no permitido");
			};
	}
	var cerrar = function () {
			localStorage.clear();
			sessionStorage.clear();
			window.location.href="/admin/cerrar_sesion";
			window.location.href="";
	}
	var validar = function () {
			var dato = localStorage.getItem("token");	
			if (dato==null || dato =="") {
				$('.html-sesion').hide();
				$('.html-sin-sesion').show();
				return false;
			}else{
				$('.html-sin-sesion').hide();
				$('.html-sesion').show();
				return true;
			};
	}
	var campos = function () {
		$('#usuario_steem').on('change', function() {
			isUserSteem($(this).val(),function (argument) {
				
			});
		});
		$('#correo_electronico').on('keyup', function() {
			validar_correo($(this).val(),function (argument) {
				
			});
		});
		
		$('#contrasena_uneeverso').on('keyup', function() {
			isValidaContrasena($(this).val());
		});
		$('#contrasena_uneeverso_2').on('keyup', function() {
			if ($('#contrasena_uneeverso').val()==$('#contrasena_uneeverso_2').val()) {

	            $("#contrasena_uneeverso_2").css('background', '#5cd867');
				if ($('#btn-acceder')) {
					$('#btn-acceder').attr('disabled',false);	
				};
				if ($('#btn-registro')) {
					$('#btn-registro').attr('disabled',false);
				};
				return true;
			} else {
	            $("#contrasena_uneeverso_2").css('background', '#ea6a6a');
				if ($('#btn-acceder')) {
					$('#btn-acceder').attr('disabled',true);	
				};
				if ($('#btn-registro')) {
					$('#btn-registro').attr('disabled',true);
				};
				return false;
	        }
		});		
	    $('#priwif_steem').on('change', function() {
	        iswif($(this).val());
	    });
	}
	var opciones_nodo_conexion = function () {
			//console.log($('#opciones_nodo_conexion').val());
			sesion.rest_con_api_steem($('#opciones_nodo_conexion').val());
	}
	var validar_correo = function (correo_) {
		var exreg_ = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
		if(exreg_.test(correo_)){
	            $("#correo_electronico").css('background', '#5cd867');
				if ($('#btn-registro')) {
					$('#btn-registro').attr('disabled',false);
				};
				return true;
			} else {
	            $("#correo_electronico").css('background', '#ea6a6a');
				if ($('#btn-registro')) {
					$('#btn-registro').attr('disabled',true);
				};
				return false;
	        }
	}
	var isUserSteem = function (usuario_steem, callback) {
			//console.log(usuario_steem);
			sesion.rest_con_api_steem(0);
			if (usuario_steem=="") {
				$("#usuario_steem").css('background', 'white');
				callback(false);
				return false
			};
			steem.api.lookupAccountNames([usuario_steem], function(err, result) {
		        //console.log(err, result[0]);
		        if (result[0]==null) {
		            $("#usuario_steem").css('background', '#ea6a6a');
					if ($('#btn-acceder')) {
						$('#btn-acceder').attr('disabled',true);	
					};
					if ($('#btn-registro')) {
						$('#btn-registro').attr('disabled',true);
					};					
					callback(false);
		        } else {
		            $("#usuario_steem").css('background', '#5cd867');
					if ($('#btn-acceder')) {
						$('#btn-acceder').attr('disabled',false);	
					};
					if ($('#btn-registro')) {
						$('#btn-registro').attr('disabled',false);
					};
					callback(true);
		        }
	    	});
	}
	var isValidaContrasena = function (contrasena_uneeverso) {
			if (contrasena_uneeverso=="") {
				$("#contrasena_uneeverso").css('background', 'white');
				return false;
			};			
			var espacio_blanco  = /[ ]/i;//No se permiten espacios en blanco
			if(!espacio_blanco.test(contrasena_uneeverso)){
	            $("#contrasena_uneeverso").css('background', '#5cd867');
				if ($('#btn-acceder')) {
					$('#btn-acceder').attr('disabled',false);	
				};
				if ($('#btn-registro')) {
					$('#btn-registro').attr('disabled',false);
				};
				return true;
			} else {
	            $("#contrasena_uneeverso").css('background', '#ea6a6a');
				if ($('#btn-acceder')) {
					$('#btn-acceder').attr('disabled',true);	
				};
				if ($('#btn-registro')) {
					$('#btn-registro').attr('disabled',true);
				};
				return false;
	        }
	}
	var iswif = function(privWif) {
			sesion.rest_con_api_steem(0);
			if (privWif=="") {
				$("#priwif_steem").css('background', 'white');
				return false;
			};			
			var isGoodWif = steem.auth.isWif(privWif);
	        if (isGoodWif === true) {
	            $("#priwif_steem").css('background', '#5cd867');
				if ($('#btn-acceder')) {
					$('#btn-acceder').attr('disabled',false);	
				};
				if ($('#btn-registro')) {
					$('#btn-registro').attr('disabled',false);
				};
				return true;
			} else {
	            $("#priwif_steem").css('background', '#ea6a6a');
				if ($('#btn-acceder')) {
					$('#btn-acceder').attr('disabled',true);	
				};
				if ($('#btn-registro')) {
					$('#btn-registro').attr('disabled',true);
				};
				return false;
	        }
	}
	var datos_sesion = function (callback) {
				var token = localStorage.getItem("token");
				if (token==null || token=="") {
					callback(false);
					return false;
				};
				var url___ = window.location.protocol+'//'+window.location.hostname+'/liberar_token';
				if (/localhost/.test(window.location.hostname)) {
					url___ = window.location.protocol+'//'+window.location.hostname+web_puerto+'/liberar_token';
				};
				$.ajax({
					url: url___,
					type:'POST',	
					data:{	
							token:token,
							key_master_public:"$sfssdf#SDFS%#$242342&564&23329&%&/%&/%",
						},
			        beforeSend: function () {},
			        success:  function (resultado) {
		        	  	//console.log(JSON.stringify(resultado));	
		        	  	callback(JSON.parse(resultado.datos));	
			        },
			    	error:  function(error) {console.log(JSON.stringify(error));}	 
				});
	}

	var extraerDatos = function () {
				sesion.rest_con_api_steem(0);

				if(sesion.validar()==false){
					return false;
				}
				sesion.datos_sesion(function (result_datos_sesion) {
	        	  	var datos_token = result_datos_sesion;
					var id_usuario = datos_token.nombre_steem;
					steem.api.getAccounts([id_usuario], function(err, result) {
						$('#opciones_nodo_conexion').hide();
						//console.log(err, result);

						var cuenta_usuario = result[0].name;
						var datos_usuario = JSON.parse(result[0].json_metadata);
						var nombreMuestra = datos_usuario.profile.name;
						var profile_image = datos_usuario.profile.profile_image;
						var cover_image = datos_usuario.profile.cover_image;
						var detalles = datos_usuario.profile.about;
						var location = datos_usuario.profile.location;
						var reputation = steem.formatter.reputation(result[0].reputation);
						var post_count = result[0].post_count;
						var sbd_balance = result[0].sbd_balance;

						// Urls
						$('.url_user').attr('href', '/@'+cuenta_usuario);
						$('.url_auto').attr('href', '/@'+cuenta_usuario+'/automatizacion');
						$('.url_segu').attr('href', '/@'+cuenta_usuario+'/seguidores');
						$('.url_sigu').attr('href', '/@'+cuenta_usuario+'/siguiendo');
						$('.url_cfg').attr('href', 'https://steemit.com/@'+cuenta_usuario+'/settings');
						$('.url_info').attr('href', 'https://steemit.com/trending/uneeverso');
 						

						//
						$('#nombreMuestra_pju').text(nombreMuestra);
						$('#nombreMuestra').text(nombreMuestra);
						if ($('#nombreMuestra_admin').length>0) {
							$('.url_user').attr('href', '/@'+cuenta_usuario);
							$('#nombreMuestra_admin').text(cuenta_usuario);
							$('#reputation_admin p').text(reputation+' %');
						}else{
							$('#reputacion_pju').text('('+reputation+')');
							//Precios de monedas
							sesion.precio_divisa("SBD",sbd_balance);
						};
						if (profile_image){
							if ($('#profile_image').length>0) {
								$('#profile_image').attr('src', profile_image);	
							}
							if ($('#profile_image_admin').length>0) {
								$('#profile_image_admin').attr('src', profile_image);
							};							
						}else{
							if ($('#profile_image').length>0) {
								$('#profile_image').hide();
							}
							if ($('#profile_image_admin').length>0) {
								$('#profile_image_admin').hide();
							};
						};

						if ($('#cover_image')) {
							$('#cover_image').css('background', 'url('+cover_image+') no-repeat');
							$('#cover_image').css('background-size', '100%');								
						};
						if ($('#cover_image_admin')) {
							$('#cover_image_admin').css('background', 'url('+cover_image+') no-repeat');
							$('#cover_image_admin').css('background-size', '100%');								
						};

						var conseguido_ ="";
						var falta_ ="";
						if (reputation>30) {
							var conseguido_ ='<span style="color:white;">Reputación '+reputation+'% </span>';
							var falta_ ='';
						}else{
							var conseguido_ ='';
							var falta_ ='<span style="color:white;"> Reputación '+reputation+'% </span>';
						};

						$('#txt-reputation').append('<a href=""><div class="progress xs">'+
												'<div id="width-reputation"'+ 
												'class="progress-bar progress-bar-green"'+
								 				'style="width: 40%"  '+
								 				'role="progressbar" aria-valuenow="0" aria-valuemin="0", aria-valuemax="100">'+
								 				conseguido_+
								 				'</div>'+
								 				falta_+
								 				'</div></a>');
						$('#width-reputation').css('width', reputation+'%');


						var secondsago = (new Date - new Date(result[0].last_vote_time + "Z")) / 1000;
						var pvoto_ = result[0].voting_power + (10000 * secondsago / 432000);
						pvoto_ = Math.min(pvoto_ / 100, 100).toFixed(2);
						//console.log("Poder de voto"+pvoto_);

						var conseguido_pv_ ="";
						var falta_pv_ ="";
						if (pvoto_>50) {
							var conseguido_pv_ ='<span style="color:white;">Poder de voto '+pvoto_+'% </span>';
							var falta_pv_ ='';					
						} else{
							var conseguido_pv_ ='';
							var falta_pv_ ='<span style="color:white;">Poder de voto '+pvoto_+'% </span>';
						};
							
							
						$('#txt-poder-voto').append('<a href=""><div class="progress xs">'+
												'<div id="width-poder-voto"'+ 
												'class="progress-bar progress-bar-aqua"'+
								 				'style="width: 40%"  '+
								 				'role="progressbar" aria-valuenow="0" aria-valuemin="0", aria-valuemax="100">'+
								 				conseguido_pv_+
								 				'</div>'+
								 				falta_pv_+
								 				'</div></a>');
						$('#width-poder-voto').css('width', pvoto_+'%');

					});
					steem.api.getFollowCount(id_usuario, function(err, result) {
						//console.log(err, result);
						$('#seguidores').text(result.follower_count);				
						$('#sigues').text(result.following_count);
					});					
				});
	}
	var url_extrar_usuario = function (para_pagina) {
				
		var url_fin = "";
		switch(para_pagina){
			case 'perfil':
				var url = window.location.pathname;
				url = url.replace('/','');
				url = url.replace('/','');
				url = url.replace('comentarios','');
				url = url.replace('respuestas','');
				url = url.replace('recompensas-autor','');
				url = url.replace('recompensas-curacion','');
				url = url.replace('monedero','');
				url = url.replace('configuracion','');
				url = url.replace('automatizacion','');
				url = url.replace('seguidores','');
				url = url.replace('siguiendo','');
				url_fin = url.replace(/[@]/gi,'');
				return url_fin;				
			break;
			case 'user_seguidores':
				var url = window.location.pathname;
				var url_2 = url.replace('seguidores','');
				var url_3 = url_2.replace('/','');
				var url_4 = url_3.replace('/','');
				url_fin = url_4.replace(/[@]/gi,'');
				return url_fin;				
			break;
			case 'user_siguiendo':
				var url = window.location.pathname;
				var url_2 = url.replace('siguiendo','');
				var url_3 = url_2.replace('/','');
				var url_4 = url_3.replace('/','');
				url_fin = url_4.replace(/[@]/gi,'');			
				return url_fin;
			break;
		}
	}	
	var rest_con_api_steem = function (opcion_select_) {
			// assuming websocket is work at ws.golos.io
			var url_api_;
			switch(opcion_select_){
				case 'api.steemit.com':
					url_api_ = 'wss://api.steemit.com';
				break;
				case 'steemd.steemit.com':
					url_api_ = 'wss://steemd.steemit.com';
				break;
				case 'steemd.privex.io':
					url_api_ = 'wss://steemd.privex.io';
				break;
				case 'steemd.minnowsupportproject.org':
					url_api_ = 'wss://steemd.minnowsupportproject.org';
				break;
				case 'steemd.pevo.science':
					url_api_ = 'wss://steemd.pevo.science';
				break;
				case 'gtg.steem.house:8090':
					url_api_ = 'wss://gtg.steem.house:8090';
				break;
				case 'seed.bitcoiner.me':
					url_api_ = 'wss://seed.bitcoiner.me';
				break;				
			}
			if (opcion_select_!=0) {
				localStorage.setItem("url_api_nodo", url_api_);
				window.location.href='';
			}else{
				if(localStorage.getItem("url_api_nodo")=="" || localStorage.getItem("url_api_nodo")==null) {
					localStorage.setItem("url_api_nodo", 'wss://steemd.privex.io');
					//localStorage.setItem("url_api_nodo", 'wss://steemd.minnowsupportproject.org');
				};
				steem.api.setOptions({ url: localStorage.getItem("url_api_nodo") }); 	
			};
			/*steem.api.login('', '', function(err, result) {
			  	//console.log(err, result);
			});	
			*/	  
	}
	//https://api.coinmarketcap.com/v1/ticker/steem-dollars/?convert=ETH
	var precio_divisa = function (divisa_,sbd_balance_) {
			var api_divisa_;
			switch(divisa_){
				case 'STEEM':
				break;
				case 'SBD':
					api_divisa_ = "https://api.coinmarketcap.com/v1/ticker/steem-dollars/?convert=ETH";
				break;
				case 'BTC':
				break;
				case 'ETH':
				break;
			}
			$.ajax({
				url: api_divisa_,
				type:'GET',
		        beforeSend: function () {},
		        success:  function (resultado) {
	        	  	//console.log(JSON.stringify(resultado));	
	        	  	var price_usd_ = JSON.stringify(resultado[0].price_usd).substring(1,5);
	        	  	var price_btc_ = resultado[0].price_btc;
	        	  	var price_eth_ = resultado[0].price_eth;
	        	  	//console.log("Precio USD: "+price_usd_);
	        	  	// Mercado
	        	  	if (/-/.test(resultado[0].percent_change_24h)) {
	        	  		$('#porcentaje_cambio').css('color','red');
	        	  	};
	        	  	$('#porcentaje_cambio').text('('+resultado[0].percent_change_24h+'%)');
	        	  	$('#precio_d_usb').text(price_usd_);
	        	  	$('#precio_d_btc').text(price_btc_);
	        	  	$('#precio_d_eth').text(price_eth_);
	        	  	// Monedero
	        	  	if (sbd_balance_!="") {
	        	  		sbd_balance_ = sbd_balance_.replace(/([A-Z])\w+/g,'');
	        	  		$('#monedero').css('display', 'block');
		        	  	//console.log(price_usd_,price_btc_,price_eth_);
		        	  	//console.log(price_usd_*sbd_balance_);
		        	  	$('#tu_sbd_balance').text(sbd_balance_);
		        	  	var m_price_usd_ = price_usd_*sbd_balance_;
		        	  	var m_price_btc_ = price_btc_*sbd_balance_;
		        	  	var m_price_eth_ = price_eth_*sbd_balance_;
		        	  	$('#monedero_precio_d_usb').text(m_price_usd_.toString().substring(0,5));
		        	  	$('#monedero_precio_d_btc').text(m_price_btc_.toString().substring(0,10));
		        	  	$('#monedero_precio_d_eth').text(m_price_eth_.toString().substring(0,12));

	        	  	};

	        	  	
		        },
		    	error:  function(error) {console.log(JSON.stringify(error));}	 
			});		
	}
	var solicitar_contrasena = function () {
			$('#btn-registro').attr('disabled', true);
			var usuario_steem_ = $("#usuario_steem").val();
			var priwif_steem_ = $("#priwif_steem").val();
			if (usuario_steem_==null || usuario_steem_==""
				|| priwif_steem_==null || priwif_steem_=="") {
				$('#btn-registro').attr('disabled', false);
				return false;
			};
			$.ajax({
				url: window.location.protocol+'//'+window.location.hostname+web_puerto+'/cuenta/solicitando_recuperar_contrasena',
				type:'POST',	
				data:{	
						nombre_steem : usuario_steem_,
						wif_post_priv_steem : priwif_steem_,
					},
		        beforeSend: function () {},
		        success:  function (resultado) {
	        	  	//console.log(JSON.stringify(resultado));	
	        	  	if (resultado.error==false) {
	        	  		alertify.alert(
							"Solicitud realizada:",
							resultado.mensaje,
							function(){
						    	localStorage.clear();
								sessionStorage.clear();
								window.location.href="/login";
						});
	        	  	} else{
	        	  		alerta.error(resultado.mensaje);
	        	  		$('#btn-registro').attr('disabled', false);
	        	  	};
		        },
		    	error:  function(error) {console.log(JSON.stringify(error));}	 
			});		
	}
	var cambiar_contrasena = function () {
			$('#btn-registro').attr('disabled', true);
			var usuario_steem_ = $('#usuario_steem').val();
			var contrasena_uneeverso_ = $('#contrasena_uneeverso').val();			
			var codigo_recuperacion_ = $('#codigo_recuperacion').val();
			if (usuario_steem_=="" || contrasena_uneeverso_=="" ||
				 codigo_recuperacion_=="" ) {
				$('#btn-registro').attr('disabled', false);
				return false;
			}
			isUserSteem(usuario_steem_, function (validacion_u_steem) {
				if (validacion_u_steem!=false) {
					if (isValidaContrasena(contrasena_uneeverso_)) {
					    $.ajax({
							url: window.location.href+'/enviar-cambio',
							type:'POST',	
							data:{	
									nombre_steem:usuario_steem_,
									contrasena_uneeverso:contrasena_uneeverso_,
									codigo_recuperacion:codigo_recuperacion_,
								},
					        beforeSend: function () {

					        },
					        success:  function (resultado) {
				        	  	//console.log(JSON.stringify(resultado));
				        	  	if (resultado.error==false) {
				        	  		
				        	  		alertify.alert(
										"Cambio realizado:",
										resultado.mensaje,
										function(){
									    	localStorage.clear();
											sessionStorage.clear();
											window.location.href="/login";
									});
				        	  		//alerta.exito(resultado.mensaje);
									
				        	  	} else{
				        	  		alerta.error(resultado.mensaje);
				        	  		$('#btn-registro').attr('disabled', false);
				        	  	};
					        },
					    	error:  function(error) {
								console.log(JSON.stringify(error));
						    }	 
						});

					} else{
						$('#btn-registro').attr('disabled', false);
						alerta.error("Contraseña uneeverso tiene espacios");
					};
				}else{
					$('#btn-registro').attr('disabled', false);
					alerta.error("Usuario steem no existe");
				};
			});
	}

	var obtener_wif_public = function () {
			var usuario_steem_ = $('#usuario_steem_keyps').val();
			var contrasena_uneeverso_ = $('#contrasena_steem_keyps').val();			
			var privActiveWif  = "";
			if (usuario_steem_=="" || contrasena_uneeverso_=="" ) {
				return false;
			}
			sesion.rest_con_api_steem(0);
			isUserSteem(usuario_steem_, function (validacion_u_steem) {
				if (validacion_u_steem!=false) {
					if(isValidaContrasena(contrasena_uneeverso_)) {
						privActiveWif  = steem.auth.toWif(usuario_steem_, contrasena_uneeverso_, 'posting');
						//console.log(privActiveWif);
						if (iswif(privActiveWif)!=false) {
							$('#priwif_steem').val(privActiveWif);	
							$('#cerrar_modal').click();
						}else{
							alerta.error("Error al obtener la contraseña de publicacíon");
						};
					} else{
						alerta.error("Su contraseña principal steem tiene espacios");
					};
				}else{
					alerta.error("El usuario ingresado no existe");
				};
			});
	}

	return{
		app:function  () {
			campos();
			var dato = localStorage.getItem("token");			
			if (dato!=null && dato !="") {

				//window.location.href='/@'+dato+'/automatizacion';
			};
		},
		iniciar : iniciar,
		registrar_usuario : registrar_usuario,
		cerrar : cerrar,
		validar : validar,
		iswif : iswif,
		datos_sesion : datos_sesion,
		isUserSteem : isUserSteem,
		isValidaContrasena : isValidaContrasena,
		validar_correo : validar_correo,
		rest_con_api_steem : rest_con_api_steem,
		extraerDatos : extraerDatos,
		url_extrar_usuario : url_extrar_usuario,
		precio_divisa : precio_divisa,
		solicitar_contrasena : solicitar_contrasena,
		cambiar_contrasena : cambiar_contrasena,
		obtener_wif_public : obtener_wif_public,
		opciones_nodo_conexion : opciones_nodo_conexion,
	}
}();