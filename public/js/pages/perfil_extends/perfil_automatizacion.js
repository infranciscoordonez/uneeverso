var perfil_automatizacion = function () {

	var activar_desactivar = function () {
		$('#activar_desactivar_btn').attr('disabled', true);
		sesion.datos_sesion(function (result_datos_sesion) {
			//console.log(result_datos_sesion);
			var usuario_steem_ = result_datos_sesion.nombre_steem;
			var activar_desactivar_ = $("#activar_desactivar").is(':checked');
			activar_desactivar_ = (activar_desactivar_==true)? 1 : 0 ;
			if (usuario_steem_=="" ) {
				return false;
			}
			sesion.isUserSteem(usuario_steem_, function (validacion_u_steem) {
				if (validacion_u_steem==true) {
				    $.ajax({
						url: window.location.href+'/activar_desactivar_automatizacion',
						type:'POST',	
						data:{	
								nombre_steem:usuario_steem_,
								activar_desactivar:activar_desactivar_,
							},
				        beforeSend: function () {

				        },
				        success:  function (resultado) {
			        	  	//console.log(JSON.stringify(resultado));
			        	  	if (resultado.error==false) {
			        	  		alerta.exito(resultado.mensaje);
				        	  	localStorage.setItem("token", resultado.token);
				        	  	validar_auto(resultado.datos);
			        	  	} else{
			        	  		alerta.error(resultado.mensaje);
			        	  	};
							$('#activar_desactivar_btn').attr('disabled', false);
				        },
					    error:  function(error) {
					    	$('#activar_desactivar_btn').attr('disabled', false);
							console.log(JSON.stringify(error));
					   }	 
					});
				}else{
					$('#activar_desactivar_btn').attr('disabled', false);
					alerta.error("EL usuario no existe en steem");
				};

			});	
		});
	}
	var guardar_url = function () {
		$('#send_url_btn').attr('disabled', true);
		sesion.datos_sesion(function (result_datos_sesion) {
			var usuario_steem_ = result_datos_sesion.nombre_steem;
			var url_post_comm_ = $('#url_post_comm').val();
			if (usuario_steem_=="" || url_post_comm_=="") {
				$('#send_url_btn').attr('disabled', false);
				return false;
			}
			//fecha_actual 
			var url_post_comm_GESTIONADA = perfil_automatizacion.limpiar_url(url_post_comm_);
			sesion.isUserSteem(usuario_steem_, function (validacion_u_steem) {
				if (validacion_u_steem==true) {					
					if (url_post_comm_GESTIONADA!=false) {
						if (url_post_comm_GESTIONADA.usuario_url == usuario_steem_) {
							steem.api.getContent(url_post_comm_GESTIONADA.usuario_url, url_post_comm_GESTIONADA.url, function(err, result) {
								//console.log(err, result);							
								console.log(result.author);
								if (result.author!="" && result.author!=null) {
									var fecha1 = moment(datos.fecha_actual());
									var fecha2 = moment(moment(result.created).format("YYYY-MM-DD"))/*.format("YYYY-MM-DD");*/;							
									var dias_diferencia = fecha1.diff(fecha2, 'days');
									if (dias_diferencia<=4){
										$.ajax({
											url: window.location.href+'/guardar_url',
											type:'POST',	
											data:{	
												nombre_steem:usuario_steem_,
												url_post_comm:url_post_comm_GESTIONADA.url,
											},
									        beforeSend: function () {

									        },
									        success:  function (resultado) {
								        	  	//console.log(JSON.stringify(resultado));
								        	  	if (resultado.error==false) {
								        	  		alerta.exito(resultado.mensaje);
									        	  	perfil_automatizacion.cargar_lista_espera_personal();
								        	  	} else{
								        	  		alerta.error(resultado.mensaje);
								        	  	};
								        	  	$('#send_url_btn').attr('disabled', false);			        	  	
									        },
									    	error:  function(error) {
												console.log(JSON.stringify(error));
												$('#send_url_btn').attr('disabled', false);
										   }	 
										});
									}else{
										$('#send_url_btn').attr('disabled', false);
										alerta.error("La url, tiene mas de 4 dias de ser publicada");
									};
								} else{
									$('#send_url_btn').attr('disabled', false);
									alerta.error("Error al obtener los datos: La url no es correcta");
								};
							});
						} else{
							$('#send_url_btn').attr('disabled', false);
							alerta.error("Esta url no le pertenece");
						};
					} else{
						$('#send_url_btn').attr('disabled', false);
						alerta.error("La url, no pertenece a una publicación de la red steem");
					};
				}else{
					$('#send_url_btn').attr('disabled', false);
					alerta.error("EL usuario no existe en steem");
				};
			});	
		});
	}


	// Cuando esta vacia de espera, muestra error en consola
	var cargar_lista_espera_personal = function () {
			var usuario_steem_ = sesion.url_extrar_usuario('perfil');
			if (usuario_steem_=="" ) {
				return false;
			}
			sesion.isUserSteem(usuario_steem_, function (validacion_u_steem) {
				if (validacion_u_steem==true) {
					    $.ajax({
							url: window.location.href+'/cargar_lista_espera_personal',
							type:'POST',	
							data:{	
									nombre_steem:usuario_steem_,
								},
					        beforeSend: function () {

					        },
					        success:  function (resultado) {
				        	  	//console.log(JSON.stringify(resultado));
				        	  	if (resultado.error==false) {
				        	  		//console.log(resultado.datos);
				        	  		if (resultado.datos.length>0) {
										$('#lista_espera_personal').html('');				
										$(resultado.datos).each(function (index_1, result_1) {
											var color_bar_progress = "";
												/*switch(){

											}				        	  	
											progress-bar-success
											bg-light-blue
											progress-bar-yellow
											progress-bar-danger
												*/


											$('#lista_espera_personal').append('<tr>'+
												'<td>'+result_1.url+'</td>'+
												/*'<td >'+
													'<div class="progress xs progress-striped active">'+
														'<div class="progress-bar '+color_bar_progress+'" style="width: 80%">'+
														'</div>'+		
													'</div>'+
												'</td>'+*/
												'<td>'+
													'<center>'+
														'<span class="badge ">'+result_1.numero_en_cola+'</span>'+
													'</center>'+
												'</td>'+
											'<tr>');
										});
									};
								};
					        },
					    	error:  function(error) {
								console.log(JSON.stringify(error));
						    }	 
						});

				}else{
					console.log("EL usuario no existe en steem");
				};
			});	
	}

	var validar_url_post = function (url_) {
		// validar seria comprobar si es o no es una url de una post
		var exreg_post = /(ftp|http|https):\/\/[a-z-. 0-9]+.+[a-z-. 0-9-.]{2,10}\/+[a-z-. 0-9-.]+\/@+[a-z-. 0-9-.]+\/[A-Z-. a-z-. 0-9-.]+/g;
		var url_new_ = url_.replace(exreg_post,'');
		return (url_new_=="")? true : false;  
	}
	var validar_url_comentario = function (url_) {
		// validar seria comprobar si es o no es una url de un comentario
		var exreg_comentario = /(ftp|http|https):\/\/[a-z-. 0-9]+.+[a-z-. 0-9-.]{2,10}\/+[a-z-. 0-9-.]+\/@+[a-z-. 0-9-.]+\/[A-Z-. a-z-. 0-9-.]+[#]+[@]+[a-z-.A-Z-.0-9-.]+\/[A-Z-. a-z-. 0-9-.]+/g;		
		var url_new_ = url_.replace(exreg_comentario,'');
		return (url_new_=="")? true : false; 
	}
	var limpiar_url = function (url_) {

		var usuario_url_ = url_;
		// Para post
		var exreg_post_url = /(ftp|http|https):\/\/[a-z-. 0-9]+.+[a-z-. 0-9-.]{2,10}\/+[a-z-. 0-9-.]+\/@+[a-z-. 0-9-.]+\//g;
		//--------
		// Para comentarios
		//var exreg_comentario_url = /(ftp|http|https):\/\/[a-z-. 0-9-.]+.+[a-z-. 0-9-.]{2,10}\/+[a-z-. 0-9]+\/@+[a-z-. 0-9-.]+\/[A-Z-. a-z-. 0-9-.]+[#]+[@]+[a-z-.A-Z-.0-9-.]+\//g;
		//--------

		if (!perfil_automatizacion.validar_url_comentario(url_)) {
			if (!perfil_automatizacion.validar_url_post(url_)) {
				return false;
			}else{
				url_ = url_.replace(exreg_post_url,'');
				usuario_url_ = usuario_url_.match(/@+[a-z-.0-9-.]+/g);
				usuario_url_ = usuario_url_[0].replace(/@/g,'');
			}			
		}else{
			/*url_ = url_.replace(exreg_comentario_url,'');
			usuario_url_ = usuario_url_.match(/#@+[a-z-.0-9-.]+/g);
			usuario_url_ = usuario_url_[0].replace(/#@/g,'');*/
			return false;
		}
		return {  
			url : url_,
			usuario_url : usuario_url_
		};
	}
	var animaciones = function () {
			 $('#url_post_comm').on('keyup',function (argument) {
			 	$('#send_url_btn').attr('disabled', false);
			 });
			$('.menu_perfil .menu-item-active').removeClass('menu-item-active').addClass('menu-item');
			$('#menu_url_7').removeClass('menu-item').addClass('menu-item-active');
			var usuario = sesion.url_extrar_usuario('perfil');
			$('#menu_2_url_1').attr('href', '/@'+usuario+'/configuracion');
			$('#menu_2_url_2').attr('href', 'javascript:;');
			//
			sesion.datos_sesion(function (argument) {
				validar_auto(argument);
			});
	}
	var validar_auto = function (result_datos_sesion) {
			if (sesion.url_extrar_usuario('perfil')==result_datos_sesion.nombre_steem) {
				if(result_datos_sesion.auto_upvotes>0){
					$("#activar_desactivar_lab div").attr('aria-checked',true);
					$("#activar_desactivar_lab div").addClass('checked');
					$("#activar_desactivar").attr('checked',true);
					$('#envio_url').show();
					$('#lista_espera').show();
					perfil_automatizacion.cargar_lista_espera_personal();
				}else{
					$("#activar_desactivar_lab div").attr('aria-checked',false);
					$("#activar_desactivar_lab div").removeClass('checked');
					$("#activar_desactivar").attr('checked',false);
					$('#envio_url').hide();
					$('#lista_espera').hide();
					//console.log("tu automatizacion esta desactivada");
				};
			}else{
				//console.log("es de otro usuario");
				$('.content').hide();
			};
	}

	return{
		Iniciar: function () {
			perfil_automatizacion.animaciones();
		},
		animaciones:animaciones,
		validar_url_post : validar_url_post,
		validar_url_comentario : validar_url_comentario,
		limpiar_url : limpiar_url,
		activar_desactivar : activar_desactivar,
		guardar_url : guardar_url,
		cargar_lista_espera_personal : cargar_lista_espera_personal,
	}
}();
perfil_automatizacion.Iniciar();


