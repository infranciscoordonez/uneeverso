var perfil = function () {



	var animaciones = function (usuario, callback) {
			//console.log("Cargando animaciones...");
			$('#menu_url_1').attr('href', '/@'+usuario);
			$('#menu_url_2').attr('href', '/@'+usuario+'/comentarios');
			$('#menu_url_3').attr('href', '/@'+usuario+'/respuestas');
			//$('#menu_url_4').attr('href', '/@'+usuario+'/recompensas-autor');
			//$('#menu_url_5').attr('href', '/@'+usuario+'/recompensas-curacion');
			$('#menu_url_6').attr('href', '/@'+usuario+'/monedero');
			$('#menu_url_7').attr('href', '/@'+usuario+'/configuracion');
			$('#sidebar_izquierdo').on('click',function (event) {
				if ($(this).hasClass('sidebar_cerrado')) {
					$(this).removeClass('sidebar_cerrado').addClass('sidebar_abierto');
					$('#divNombreMuestra_perfil').css('margin-left','19px');
					$('#atc_steem').css('margin-left','54px');
				}else{
					$(this).removeClass('sidebar_abierto').addClass('sidebar_cerrado');
					$('#divNombreMuestra_perfil').css('margin-left','');
					$('#atc_steem').css('margin-left','19px');
				}				
			});
			callback(true);
	}
	var cargar_datos = function (usuario) {

			//console.log("Cargando datos...");
			steem.api.getAccounts([usuario], function(err, result) {
				//console.log(err, result);
				if (err) {
					console.log(err);
				} else{
					$('#opciones_nodo_conexion').hide();
					//console.log(err, result);
					if (result.length>0) {
						if (result[0].json_metadata) {

							var cuenta_usuario = result[0].name;
							var datos_usuario = JSON.parse(result[0].json_metadata);
							var nombreMuestra = datos_usuario.profile.name;
							var profile_image = datos_usuario.profile.profile_image;
							var cover_image = datos_usuario.profile.cover_image;
							var detalles = datos_usuario.profile.about;
							var location = datos_usuario.profile.location;
							var website = datos_usuario.profile.website;
							var reputation = steem.formatter.reputation(result[0].reputation);
							var post_count = result[0].post_count;

							//
							$('#nombreMuestra_perfil').text(nombreMuestra);
							$('#atc_steem_posts').text(post_count);
							$('#reputacion_perfil').text('('+reputation+')');
							if (profile_image) {						
								$('#profile_image_perfil').css('background', 'url('+profile_image+') no-repeat');
								$('#profile_image_perfil').css('background-size', 'cover')
								//$('#profile_image_perfil').attr('src', profile_image);
								$('#profile_image_perfil').css('display', 'block');
							};
							if (cover_image) {
								$('#cover_image_perfil').css('background', 'url('+cover_image+') no-repeat');
							};
							//$('#cover_image_perfil').css('background-size', '100% 150px');
							$('#cover_image_perfil').css('background-size', 'cover');
							$('#cover_image_perfil').css('background-position', '50% 50%');
							$('#descripcion_sobre_perfil').text(detalles);


							if (location) {
								$('#dato_localizacion').text(location);
								$('#dato_localizacion').attr('src',location);
								$('#dato_localizacion_icon').css('display', 'block')
							}
							if (website) {
								$('#dato_pagina_web').text(website);
								$('#dato_pagina_web').attr('src',website);
								$('#dato_pagina_web_icon').css('display', 'block')
							}
							steem.api.getFollowCount(usuario, function(err, result) {
								//console.log(err, result);
								$('#atc_steem_seguidores').text(result.follower_count);				
								$('#atc_steem_siguiendo').text(result.following_count);
							});			
						} else{
							$('#nombreMuestra_perfil').text(usuario);
						}  
					}else{
						window.location.href='/noexiste@'+usuario;
					};
				};
			});

	}


	return{
		Iniciar: function () {
			var usuario = sesion.url_extrar_usuario('perfil');
			setTimeout(function () {
				perfil.animaciones(usuario,function (result) {
					perfil.cargar_datos(usuario);
				});	
			}, 3000);
		},
		animaciones:animaciones,
		cargar_datos:cargar_datos,
	}
}();

	perfil.Iniciar();


