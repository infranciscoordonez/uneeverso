var datos = function () {
 
	var exportar = function () {
			var key_public = $("#key_public").val();
			if (key_public==null || key_public=="") {
				return false;
			};
			window.location.href=window.location.href+'/exportar/'+key_public;
	}
	var importar = function () {
			$('#migrar_datos').unbind('submit').bind('submit',function(e){
			    e.preventDefault();				
				var form = $(this);

		        var key_public = $("#key_public").val();
		        var file2 = document.getElementById("archivodb");   //TENGO DUDA
		        var archivo = file2.files[0]; 				
	            if(key_public && archivo){
	            	// Crea un formData y lo envías
			        var formData = new FormData(form);
			        formData.append('key_public',key_public);
			        formData.append('archivodb',archivo);
				    $.ajax({
						url : form.attr('action'),
		                type : form.attr('method'),
		                data : formData,
		                data: formData,
			            cache: false,
			            contentType: false,
			            processData: false,
				       	success: function(response){
				       		console.log(JSON.stringify(response));
				       	},
				       	error:  function(error) {
							console.log(JSON.stringify(error));
						}		 
					});
				}
    			return false;
			});
	}	

	var fecha_actual = function () {
		var d = new Date();
		return d.getFullYear()+'-'+d.getMonth()+1+'-'+d.getDate();
	} 
	return{
		Iniciar: function () {
			
			datos.importar();
		},
		exportar : exportar,
		importar : importar,
		fecha_actual : fecha_actual,
	}
}();
$(window).load(function () {
	datos.Iniciar();
});

