//creamos la base de datos tienda y el objeto USUARIO donde iremos almacenando la info
var ENCRIPTACION = require("../routes/utilidades/encriptacion");
var USUARIO = {};

//elimina y crea la tabla usuario
USUARIO.crearTabla = function()
{
	//db.run("DROP TABLE IF EXISTS usuario"); //Esto es peligroso, porque eliminaria datos existentes
	db.run("CREATE TABLE [usuario]("+
    "[id] INTEGER PRIMARY KEY AUTOINCREMENT, "+
    "[nombre_steem] TEXT, "+
    "[contrasena_uneeverso] TEXT, "+
    "[wif_post_priv_steem] TEXT, "+
    "[auto_upvotes] INT, "+
    "[correo_electronico] TEXT, "+
    "[codigo_recuperacion] TEXT,"+
    "[id_perfil] INT);");
	console.log("La tabla usuario ha sido correctamente creada");
}

// Iniciar sesion
USUARIO.get_iniciar_sesion = function(db,datos,callback)
{
    var sql = "SELECT * FROM usuario WHERE nombre_steem = ? AND contrasena_uneeverso = ?";
	var stmt =  db.get(sql,[datos.nombre_steem,ENCRIPTACION.encrypt(datos.contrasena_uneeverso, datos.contrasena_uneeverso)],
                    function (error,row) {
                    //console.log(error, row);
                    if(error) 
                    {
                        throw error;
                    }else{
                        //retornamos la fila con los datos del usuario
                        if(row) 
                        {
                            callback(error, row);
                        }else{
                            //El usuario no existe en uneeverso
                            callback(error, false);
                        }
                    }
                });
}
// Iniciar sesion
USUARIO.verificar_credenciales = function(db,datos,callback)
{
    var sql = "SELECT * FROM usuario WHERE nombre_steem = ? AND wif_post_priv_steem = ?";
    var stmt =  db.get(sql,[datos.nombre_steem, datos.wif_post_priv_steem],
                    function (error,row) {
                    //console.log(error, row);
                    if(error) 
                    {
                        throw error;
                    }else{
                        //retornamos la fila con los datos del usuario
                        if(row) 
                        {
                            callback(error, row);
                        }else{
                            //El usuario no existe en uneeverso
                            callback(error, false);
                        }
                    }
                });
}
//obtenemos un usuario por su id, en este caso hacemos uso de db.get
//ya que sólo queremos una fila
USUARIO.get_u_nombre = function(db,datos,callback)
{
	//stmt = db.prepare();
	//pasamos el id del cliente a la consulta
    //stmt.bind(); 
    db.get("SELECT * FROM usuario WHERE nombre_steem = ?",[datos.nombre_steem],function(error, row)
    {
    	if(error) 
        {
            throw error;
        }else{
        	//retornamos la fila con los datos del usuario
            if(row) 
            {
                //console.log(row);
                callback(error, row);
            }else{
            	//El usuario no existe en uneeverso
            	callback(error, false);
            }
        }
    });
}

//inserta un nuevo usuario
USUARIO.registrar_u_steem = function(db,datos,callback)
{   
	var stmt = db.prepare("INSERT INTO usuario VALUES (?,?,?,?,?,?,?,?)");
	stmt.run(
				null,
				datos.nombre_steem,
				ENCRIPTACION.encrypt(datos.contrasena_uneeverso,datos.contrasena_uneeverso),
				datos.wif_post_priv_steem,
                1,
                datos.correo_electronico,
                0,
                3
			);
	stmt.finalize();
	callback(true);
}

//Vaciar datos de la tabla
USUARIO.vaciar_tabla = function()
{
	db.run("DELETE FROM USUARIO");
	console.log("Tabla usuario vacia");
}


//obtenemos todos los usuario de la tabla usuario
//con db.all obtenemos un array de objetos, es decir todos
USUARIO.get_usuarios = function(db,callback)
{
	db.all("SELECT nombre_steem, wif_post_priv_steem FROM usuario WHERE auto_upvotes=1", function(error, rows) {
        if(error) 
        {
            throw error;
        }else{
            if(rows) 
            {
                callback(error, rows);
            }else{
                //console.log(error,row);
                // no hay resultados
                callback(error, false);
            }
        }
	});
}



USUARIO.gestion_codigo_recuperacion = function(db,codigo_recuperacion_,nombre_steem_,callback)
{
    var sql = "UPDATE usuario SET codigo_recuperacion=?  WHERE nombre_steem=? ";
    db.run(sql,
            [codigo_recuperacion_,nombre_steem_],
            function (error) {
                callback(error);    
            });
}

USUARIO.gestion_codigo_r_contrasena = function(db,datos_,callback)
{
    var sql = "UPDATE usuario SET codigo_recuperacion=0, contrasena_uneeverso=?  WHERE nombre_steem=? AND codigo_recuperacion=?";
    
    console.log("NUEVA CONTRASEÑA ENCRIPTACION: "+ENCRIPTACION.encrypt(datos_.contrasena_uneeverso,datos_.contrasena_uneeverso));
    db.run(sql,
            [
                ENCRIPTACION.encrypt(datos_.contrasena_uneeverso,datos_.contrasena_uneeverso),  
                datos_.nombre_steem,
                datos_.codigo_recuperacion                
            ],
            function (error) {
                callback(error);    
            });
}

/***********************************************/
//Administración


//obtenemos todos los usuario de la tabla usuario
//con db.all obtenemos un array de objetos, es decir todos
USUARIO.get_a_usuarios = function(db,callback)
{
    db.all("SELECT * FROM usuario", function(error, rows) {
        if(error) 
        {
            throw error;
        }else{
            if(rows) 
            {
                callback(error, rows);
            }else{
                //console.log(error,row);
                // no hay resultados
                callback(error, false);
            }
        }
    });
}


USUARIO.editar = function(db,datos_,callback)
{
    var sql = "UPDATE usuario SET nombre_steem=?, correo_electronico=?, wif_post_priv_steem=?, auto_upvotes=?, id_perfil=?  WHERE id=? ";
    db.run(sql,
            [
                datos_.nombre_steem,
                datos_.correo_electronico,
                datos_.wif_post_priv_steem,
                datos_.auto_upvotes,
                datos_.id_perfil,
                datos_.id_usuario,
            ],
            function (error) {
                callback(error);    
            });
}







//exportamos el modelo para poder utilizarlo con require
module.exports = USUARIO;