//creamos la base de datos tienda y el objeto USUARIO donde iremos almacenando la info
var ENCRIPTACION = require("../routes/utilidades/encriptacion");
var UTILIDAD = require('../mod/utilidad');
var GESTION_URL = {};

//elimina y crea la tabla usuario
GESTION_URL.crearTabla = function()
{
	//db.run("DROP TABLE IF EXISTS usuario"); //Esto es peligroso, porque eliminaria datos existentes
	db.run("CREATE TABLE IF NOT EXISTS gestion_url (id INTEGER PRIMARY KEY AUTOINCREMENT, url TEXT,nombre_steem TEXT, fecha_registro DATETIME, fecha_votacion DATETIME, estado INT )");
	console.log("La tabla gestion_url ha sido correctamente creada");
}


GESTION_URL.guardar = function(db,datos,callback)
{
    console.log("guardando url"+ new Date);
    //Estado 1 = En espera/cola
    var sql = "INSERT INTO gestion_url (url, nombre_steem, fecha_registro, estado)VALUES(?,?,?,?)";
    db.run(sql,
            [datos.url_post_comm,datos.nombre_steem,UTILIDAD.fecha_actual_(), 1], 
            function (error) {
                console.log("GUARDADO");
                console.log(error);
                callback(error);    
    });
}
GESTION_URL.verificar_espera = function(db,datos,callback)
{
    var sql = "SELECT * FROM gestion_url WHERE nombre_steem = ? AND url = ? AND estado=1";
	var stmt =  db.get(sql,[datos.nombre_steem,datos.url_post_comm],
                    function (error,row) {
                    //console.log(error, row);
                    if(error) 
                    {
                        throw error;
                    }else{
                        if(row) 
                        {
                            console.log(error,row);
                            callback(error, row);
                        }else{
                            console.log(error,row);
                            // no hay resultados
                            callback(error, false);
                        }
                    }
                });
}
GESTION_URL.cargar_lista_espera_personal = function(db,datos,callback)
{
    // el +1 de la sub consulta es porque se cuentan los que estan delante de la cola, pero se necesita conocer, el numero de la url en la cola.
    var sql =   " SELECT "+ 
                        " (SELECT "+
                        "   COUNT(g_u_sub.id) as total "+
                        " FROM gestion_url AS g_u_sub "+
                        " WHERE g_u_sub.fecha_registro < g_u.fecha_registro AND g_u_sub.estado = 1)+1 as numero_en_cola, "+
                       " g_u.id, "+
                       " g_u.nombre_steem, "+
                       " g_u.url, "+
                       " g_u.fecha_registro, "+
                       " g_u.estado "+
                " FROM gestion_url AS g_u "+
                    " INNER JOIN usuario AS u ON u.nombre_steem = g_u.nombre_steem "+
                " WHERE g_u.nombre_steem = ? AND g_u.estado = 1 "+
                " ORDER BY g_u.id ASC ";
    var stmt =  db.all(sql,[datos.nombre_steem],
                    function (error,row) {
                    //console.log(error, row);
                    if(error) 
                    {
                        throw error;
                    }else{
                        if(row) 
                        {
                            callback(error, row);
                        }else{
                            console.log(error,row);
                            // no hay resultados
                            callback(error, false);
                        }
                    }
                });
}




GESTION_URL.activar_desactivar_automatizacion = function(db,datos,callback)
{
    //Estado 1 = En espera/cola
    var sql = "UPDATE usuario SET auto_upvotes=? WHERE nombre_steem=? ";
    db.run(sql,
            [datos.activar_desactivar,datos.nombre_steem],
            function (error) {
                console.log("activar/desactivar");
                console.log(error);
                callback(error);    
            });
}
GESTION_URL.verificar_activar_desactivar_automatizacion = function(db,datos,callback)
{
    var sql = "SELECT * FROM usuario WHERE nombre_steem = ? AND auto_upvotes = ?";
    var stmt =  db.get(sql,[datos.nombre_steem,datos.activar_desactivar],
                    function (error,row) {
                    //console.log(error, row);
                    if(error) 
                    {
                        throw error;
                    }else{
                        if(row) 
                        {
                            console.log(error,row);
                            callback(error, row);
                        }else{
                            console.log(error,row);
                            // no hay resultados
                            callback(error, false);
                        }
                    }
                });
} 


GESTION_URL.verificar_automatizacion = function (db,datos,callback) {
    // el +1 de la sub consulta es porque se cuentan los que estan delante de la cola, pero se necesita conocer, el numero de la url en la cola.
    var sql =   " SELECT  "+
                "    * "+
                " FROM gestion_url AS g_u "+
                " WHERE g_u.url = ? AND nombre_steem=? AND estado=2";
    var stmt =  db.get(sql,[datos.url_post_comm, datos.nombre_steem],
                    function (error,row) {
                    //console.log(error, row);
                    if(error) 
                    {
                        throw error;
                    }else{
                        if(row) 
                        {
                            callback(error, row);
                        }else{
                            console.log(error,row);
                            // no hay resultados
                            callback(error, false);
                        }
                    }
                });   
}
GESTION_URL.verificar_post_dia = function (db,datos,callback) {
    // el +1 de la sub consulta es porque se cuentan los que estan delante de la cola, pero se necesita conocer, el numero de la url en la cola.
    var sql =   " SELECT  "+
                "    MAX(g_u.id), "+
                "    g_u.id, "+
                "    g_u.nombre_steem as nombre_steem, "+
                "    g_u.url, "+
                "    g_u.fecha_registro, "+
                "    g_u.estado "+
                " FROM gestion_url AS g_u "+
                " WHERE g_u.nombre_steem = ? ";
    var stmt =  db.all(sql,[datos.nombre_steem],
                    function (error,row) {
                    //console.log(error, row);
                    if(error) 
                    {
                        throw error;
                    }else{
                        if(row) 
                        {
                            callback(error, row);
                        }else{
                            console.log(error,row);
                            // no hay resultados
                            callback(error, false);
                        }
                    }
                });   
}
GESTION_URL.automatizador_ON = function (db,callback) {
    // el +1 de la sub consulta es porque se cuentan los que estan delante de la cola, pero se necesita conocer, el numero de la url en la cola.
    var sql =   "  SELECT "+ 
                        " (SELECT     "+
                        "  COUNT(g_u.id)  "+
                        " FROM gestion_url AS g_u "+
                        " WHERE g_u.estado = 1) as cant_espera, "+
                        " (SELECT "+
                        " COUNT(g_u_sub.id) as total "+
                        " FROM gestion_url AS g_u_sub "+
                        " WHERE g_u_sub.fecha_registro < g_u.fecha_registro AND g_u_sub.estado = 1)+1 as numero_en_cola, "+
                        " g_u.id, "+
                        " g_u.nombre_steem as nombre_steem, "+
                        " g_u.url, "+
                        " g_u.fecha_registro, "+
                        " g_u.estado, "+
                        " g_u.cant_voto "+
                 " FROM gestion_url AS g_u "+
                 " WHERE g_u.estado = 1  AND (SELECT "+
                    "  COUNT(g_u_sub.id) as total "+
                    "  FROM gestion_url AS g_u_sub "+
                    "  WHERE g_u_sub.fecha_registro < g_u.fecha_registro AND g_u_sub.estado = 1)+1 = 1 "+
                 " ORDER BY g_u.id ASC ";
    var stmt =  db.get(sql,
                    function (error,row) {
                    //console.log(error, row);
                    if(error) 
                    {
                        throw error;
                    }else{
                        if(row) 
                        {
                            callback(error, row);
                        }else{
                            console.log(error,row);
                            // no hay resultados
                            callback(error, false);
                        }
                    }
                });   
}
GESTION_URL.despachar_url_gestionada = function(db,nombre_steem_,url_,callback)
{
    var sql = "SELECT * FROM gestion_url WHERE nombre_steem = ? AND url = ?";
    var stmt =  db.get(sql,[nombre_steem_,url_],
                    function (error,row) {
                    //console.log(error, row);
                    if(error) 
                    {
                        throw error;
                    }else{
                        if(row) 
                        {
                            var sql = "UPDATE gestion_url SET estado=2, cant_voto=?, fecha_votacion=?  WHERE nombre_steem=? AND url=? ";
                            db.run(sql,
                                    [row.cant_voto+1,UTILIDAD.fecha_actual_(),nombre_steem_,url_],
                                    function (error) {
                                        callback(error);    
                                    });
                        }else{
                            console.log("Error inesperado en url validada y en proceso de upvotos");
                        }
                    }
                });
}


//exportamos el modelo para poder utilizarlo con require
module.exports = GESTION_URL;