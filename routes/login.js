var ENCRIPTACION = require("../routes/utilidades/encriptacion");
var DBCONEXION = require('../mod/conexion');
var conexion_db_ON;
var USUARIO = require('../mod/usuario');
var nodemailer = require('nodemailer');
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
    res.render('login.jade');
});

router.post('/iniciar_sesion', function(req_, res_) {
	console.log(req_.body);
	DBCONEXION.iniciar_conexion(function (conexion_db) {
		if (conexion_db!=false) {
			conexion_db_ON = conexion_db;
			USUARIO.get_u_nombre(conexion_db_ON,req_.body,function(err_0,result_0){
				//console.log(err_0,result_0);
		        if(err_0)
		        {
		            res_.json(err_0);
		        }else{
		        	if (result_0==false) {
						res_.json({
		        					error : true,
		        					error_m : err_0,
		        					mensaje : "El usuario no existe en uneeverso"
		        				});
		        	}else{
		        		USUARIO.get_iniciar_sesion(conexion_db_ON,req_.body,function(error,result){
							console.log(error,result);
					        if(error)
					        {
					            res_.json(error);
					        }else{
					        	if (result==false) {
					        		res_.json({
					        					error : true,
					        					error_m : error,
					        					mensaje : "Contraseña incorrecta."
					        				});
					        	}else{
				        			res_.json({
				        					error : false,
				        					mensaje : "Inicio de sesión existoso.",
				        					datos : result,
				        					token : ENCRIPTACION.encryptUneeverso(JSON.stringify(result))
				        				});
					        	};
					            
					        }
					    });
		        	};
		            
		        }
		    });	
		};
	});	    
});

router.get('/cerrar_sesion', function(req, res) {
	req.session.id_perfil = null;
});
  
module.exports = router;
