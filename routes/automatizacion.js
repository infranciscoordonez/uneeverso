var steem = require('steem');
var url_api_nodo_ = 'wss://steemd.privex.io';
 
var ENCRIPTACION = require("../routes/utilidades/encriptacion");
var UTILIDAD = require('../mod/utilidad');
var DBCONEXION = require('../mod/conexion');
var conexion_db_ON;
var USUARIO = require('../mod/usuario');
var GESTION_URL = require('../mod/gestion_url');
var express = require('express');
var router = express.Router();
//--------------Variables de control----------------------
//Si se cambian estas variables: Variara el tiempo
var cant_minuto_vot_url = 3;// Minutos para cada ronda de votacion, por url.
var cant_url_upvot = 200;// Limite de url para obtener upvotos por RONDA GLOBAL.
/* DETALLES DEL BOT : 3 min y 200 url a votar = 
						*   Cada 200 url en 10 horas
						* 		Cada url 170 votos c/u 
						*   si la cantidad de usuarios es menor al limite de votos por url,
										el limite de votos son la cantidad actual de usuarios.
						*   10 seg de descanso para el BOT entre los votos.
*/
//--------------------------------------------------------

router.get('/', function(req, res) {

    res.render('perfil_extends/perfil_automatizacion.jade');
});
router.post('/cargar_lista_espera_personal', function(req_, res_) {
	//console.log(req_.body);
	//DBCONEXION.iniciar_conexion(function (conexion_db) {
	//	if (conexion_db!=false) {
			GESTION_URL.cargar_lista_espera_personal(conexion_db_ON,req_.body,function(err_0,result_0){
				//console.log(err_0,result_0);
				if(err_0)
				{
					res_.json(err_0);
				}else{			
					if (result_0) {
						res_.json({
		    					error : false,
								mensaje : "Existen "+result_0.length+" url en espera de upvotes.",
								datos : result_0
		    				});
					} else{
						res_.json({
								error : true,
		    					error_m : err_0,
		    					mensaje : "Actualmente, no tienes url en espera de upvotes."
							});
					};
				}
			});
    //	};
	//});
});
router.post('/activar_desactivar_automatizacion', function(req_, res_) {
	console.log(req_.body);
	GESTION_URL.activar_desactivar_automatizacion(conexion_db_ON,req_.body,function (result_2) {
		GESTION_URL.verificar_activar_desactivar_automatizacion(conexion_db_ON,req_.body,function(err_3,result_3){
			if (result_3!=false) {
				console.log(JSON.stringify(result_3));
				res_.json({
    					error : false,
    					mensaje : "Cambio de estado exitoso",
    					datos : result_3,
    					token : ENCRIPTACION.encryptUneeverso(JSON.stringify(result_3))
    				});
			} else{
				console.log(JSON.stringify(err_3),JSON.stringify(result_3));
				res_.json({
					error : true,
					error_m : err_3,
					mensaje : req_.body.nombre_steem+" No se activo/desactivo la automatización"
				});
			};
		});
	});       
});
router.post('/guardar_url', function(req_, res_) {
	console.log(req_.body);
	GESTION_URL.verificar_automatizacion(conexion_db_ON,req_.body,function(err_,result_){
		//console.log(result_);
		if (err_) {
				res_.json(err_);				
		}else{
			if (result_==false) {
				GESTION_URL.verificar_post_dia(conexion_db_ON,req_.body,function(err_,result_){
					//console.log(err_,result_);
					if(err_)
					{
						res_.json(err_);
					}else{ 
						if(result_[0].fecha_registro == UTILIDAD.fecha_actual_())
						{
							res_.json({
								error : true,
								error_m : err_,
								mensaje : "Disculpe, el dia de hoy ya envio una publicación a ser automatizada. Vuelva a intentar mañana."
							});				
						}else{

							USUARIO.get_u_nombre(conexion_db_ON,req_.body,function(err_0,result_0){
								//console.log(err_0,result_0);
							    if(err_0)
							    {
							        res_.json(err_0);
							    }else{
							    	if (result_0==false) {
										res_.json({
													error : true,
													error_m : err_0,
													mensaje : "El usuario no existe en uneeverso"
												});
							    	}else{
										GESTION_URL.verificar_espera(conexion_db_ON,req_.body,function(err_1,result_1){
											//console.log(err_1,result_1);
									        if(err_1)
									        {
									            res_.json(err_1);
									        }else{
									        	if (result_1==false) {
									        		GESTION_URL.guardar(conexion_db_ON,req_.body,function (result_2) {
														GESTION_URL.verificar_espera(conexion_db_ON,req_.body,function(err_3,result_3){
															if (result_3!=false) {
																//console.log(JSON.stringify(result_3));
																res_.json({
											        					error : false,
											        					mensaje : "Envio existoso, su url se encuentra en espera",
											        					datos : result_3
											        				});
																//DBCONEXION.cerrar_conexion(conexion_db);
															} else{
																console.log(JSON.stringify(err_3),JSON.stringify(result_3));
																//DBCONEXION.cerrar_conexion(conexion_db);
																res_.json({
										        					error : true,
										        					error_m : err_3,
										        					mensaje : "La url no fue registrada en uneeverso"
										        				});
															};
														});
													});
									        	}else{
									        		//DBCONEXION.cerrar_conexion(conexion_db);
									        		switch(result_1.estado){
									        			case 1 :					        				
											        		res_.json({
											        					error : true,
											        					error_m : err_1,
											        					mensaje : "La url se encuenta en espera, de los upvotes del bot de uneeverso",
											        				});
									        			break;
									        			case 2 :
															steem.api.getActiveVotes(req_.body.nombre_steem, req_.body.url_post_comm, function(err, result) {
																//console.log(err, result);
																res_.json({
											        					error : true,
											        					error_m : err_1,
											        					mensaje : "La url ya fue votada anteriormente por el bot de uneeverso, y llego a: '"+result.length+"' votos",
											        				});
															});
									        			break;
									        		}

									        	};
									            
									        }
									    });
							    	};
							        
							    }
							});	

						}
					};
				});


			}else{
				res_.json({
					error : true,
					error_m : err_,
					mensaje : "La url ya fue automatizada."
				});					
			};


		};

	});
});


function automatizador_2 () {
			
			GESTION_URL.automatizador_ON(conexion_db_ON,function(err_0,result_0){
				if(err_0)
				{
					console.log("Fecha de error: "+ new Date);
				}else{
					if (result_0) {							
						USUARIO.get_usuarios(conexion_db_ON,function (err_1,result_1) {
							if(err_1)
								{
									console.log("Fecha de error: "+ new Date);
								}else{				
									if (result_1) {
										var contador_votos = 0;
										var cant_limite_votantes=cant_minuto_vot_url*60-10;// 170 votos por url (10 segundosse reservan para que descanse el bot)
										//Si la cantidad de usuarios es menor al limite, se coloca la cantidad menor.(Para redicir ciclos inecesarios)
										if (result_1.length<cant_limite_votantes) {
											cant_limite_votantes =result_1.length;
										};
										console.log("Cantidad de votantes : "+cant_limite_votantes);
										var control_votos_url= setInterval(function () {
												if (result_0.nombre_steem!=result_1[contador_votos].nombre_steem) {
									 				console.log("URL    :"+result_0.url);
									 				console.log("Autor  :"+result_0.nombre_steem);
									 				console.log('contador_votos:'+contador_votos);
									 				console.log("Votante:"+result_1[contador_votos].nombre_steem);
													steem.api.setOptions({ url: url_api_nodo_ }); // assuming websocket is work at ws.golos.io
													steem.broadcast.vote(
														result_1[contador_votos].wif_post_priv_steem,result_1[contador_votos].nombre_steem,
														result_0.nombre_steem,result_0.url,
													   3000,
													   function(err_2, result_2) {
														 		if (err_2) {
														 			console.log("Fecha de error: "+ new Date);
														 		}else{
													 				console.log(result_2);
														 			GESTION_URL.despachar_url_gestionada(conexion_db_ON,
														 				result_0.nombre_steem,result_0.url,function (result_3) {
														 				console.log(result_3);
													 			});
													 		};
													});

												};
												contador_votos++;// Control de contador de votos 
												if (contador_votos==cant_limite_votantes) {
													clearTimeout(control_votos_url);
													contador_votos = 0;
												};												
										},3000);// cada segundo (segun cant_limite_votantes )
									}else{
										console.log("No hay usuario en disponible para upvote: "+ new Date);
									}
								};

						});

					} else{
						console.log("No hay url en espera: "+ new Date);
					};
				}
			});

};

DBCONEXION.iniciar_conexion(function (conexion_db) {

		if (conexion_db!=false) {
			conexion_db_ON = conexion_db;
			setInterval(function(){    
				console.log('BOT ACTIVADO');
				var contador = 0;

				var gestion_url = setInterval(function(){    
					contador++;
					console.log('NUEVA URL - N:'+contador);
					automatizador_2();
					if (contador==cant_url_upvot) {
						clearTimeout(gestion_url);
						contador = 0;
					};					
				},cant_minuto_vot_url*60*1000);// 180.000 (milisegundos) = cada 3 min (3 minutos para rondas de votacion)
			},cant_minuto_vot_url*60*1000*cant_url_upvot);//36.000.000(milisegundos) = CADA 10 HORAS (200 url a revisar)
			/*********************************************************/
			//-----------------ACTIVAR BOT AL INICIAR------------------
				console.log('BOT ACTIVADO - LUEGO DE 3 MIN DE INICIAR SERVIDOR');
				var contador_ini = 0;

				var gestion_url_ini = setInterval(function(){
					contador_ini++;
					console.log('NUEVA URL - N:'+contador_ini);
					automatizador_2();
					if (contador_ini==cant_url_upvot) {
						clearTimeout(gestion_url_ini);
						contador_ini = 0;
					};					
				},cant_minuto_vot_url*60*1000);// 180.000 (milisegundos) = cada 3 min
			/*********************************************************/
		}else{

		}
});

module.exports = router;
