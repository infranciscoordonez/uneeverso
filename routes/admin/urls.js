var ENCRIPTACION = require("../../routes/utilidades/encriptacion");
var DBCONEXION = require('../../mod/conexion');
var conexion_db_ON;
var USUARIO = require('../../mod/usuario');
var nodemailer = require('nodemailer');
var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
	if(req.session.id_perfil && req.session.id_perfil==1 || 
		req.session.id_perfil && req.session.id_perfil==2){
		res.render('admin/urls.jade', {id_perfil: req.session.id_perfil});
	}else{
		res.redirect('/admin');
	}
});

module.exports = router;
