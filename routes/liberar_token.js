var ENCRIPTACION = require("../routes/utilidades/encriptacion");
var express = require('express');
var router = express.Router();

router.post('/', function(req_, res_) {
	//console.log(req_.body);
	if (req_.body.key_master_public!="") {
		if (req_.body.key_master_public=="$sfssdf#SDFS%#$242342&564&23329&%&/%&/%") {
			res_.json({
					error : false,
					datos : ENCRIPTACION.decryptUneeverso(req_.body.token),
				});
		}else{
			res_.json({error : true, mensaje : "Contraseña maestra publica erronea"});
		}
	}else{
		res_.json({error : true, mensaje : "Contraseña maestra publica vacia"});
	};
});

module.exports = router;
