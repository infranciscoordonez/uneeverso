var DBCONEXION = require('../mod/conexion');
var conexion_db_ON;
var USUARIO = require('../mod/usuario');
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
    res.render('registro.jade');
});


//http://localhost:3000/insertUser
//Registro de un nuevo usuario en uneeverso
router.post("/registrar_usuario", function(req, res)
{	
	DBCONEXION.iniciar_conexion(function (conexion_db) {
		if (conexion_db!=false) {
			conexion_db_ON = conexion_db;
			//console.log(req.body);
			USUARIO.get_u_nombre(conexion_db_ON,req.body,function(err,result){
				//console.log(err,result);
		        if(err)
		        {
		        	DBCONEXION.cerrar_conexion(conexion_db_ON);
		            res.json(err);
		        }else{
		        	if (result==false) {
		        		USUARIO.registrar_u_steem(conexion_db_ON,req.body,function (result_r) {
							USUARIO.get_u_nombre(conexion_db_ON,req.body,function(err_v,result_v){
								if (result_v!=false) {
									//console.log(JSON.stringify(result_v));
									DBCONEXION.cerrar_conexion(conexion_db_ON);
									res.json({
				        					error : false,
				        					mensaje : "Registro existoso",
				        					datos : result_v
				        				});
								} else{
									console.log(JSON.stringify(err_v),JSON.stringify(result_v)); 
									DBCONEXION.cerrar_conexion(conexion_db_ON);
									res.json({
			        					error : true,
			        					error_m : err_v,
			        					mensaje : "El usuario no fue registrado en uneeverso"
			        				});
								};
							});
						});
		        	}else{
		        		DBCONEXION.cerrar_conexion(conexion_db_ON);
		        		res.json({
		        					error : true,
		        					error_m : err,
		        					mensaje : "El usuario ya esta registrado en uneeverso"
		        				});
		        	};
		            
		        }
			});	
		}
	});
});
	

module.exports = router;


