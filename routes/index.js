var express = require('express');
var router = express.Router();


router.get('/', function(req, res) {
    res.render('index.jade');
});

router.get('/noexiste:pagina_sin_existencia', function(req, res) {
    res.render('error_noexiste.jade',{
            message: "err.message",
            error: "sin datos"
        });
});
router.get('volver', function(req, res) {
   // res.render('index.jade');
});
module.exports = router;
